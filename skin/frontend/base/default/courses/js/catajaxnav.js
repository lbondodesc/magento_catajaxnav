
jQuery(document).ready(initSlider);
/*
if (document.readyState == 'complete') {
	initSlider();
}
*/



function setAjaxLocation(location) 
{
	 var index = location.indexOf("?");
	 var newUrl = catAjaxNavUrl;
	 if (-1 !== index) {
	  	newUrl += location.substr(index)
	 }
	 showLoadingBox();
	/* jQuery('#category_products_container').html('Loading...');*/
	 /*Replace it*/
	 jQuery('#category_products_container').load(newUrl, hideLoadingBox);
	 return false;
}

function setLayerAjaxLocation(location, data) 
{
	 var index = location.indexOf("?");
	 var newUrl = layerCatAjaxNavUrl;
	 if (-1 !== index) {
	  	newUrl += location.substr(index)
	 }
	 showLoadingBox();
	
	 if (typeof(data) != 'object') {
		 data = {};
	 }
	 
	 new Ajax.Request(newUrl,
			 {
			  method: 'get',
			  parameters: data,
			  onComplete: function(response) {
					    var json = response.responseText.evalJSON();
					    setTimeout(function() {
					    		$('category_products_container').update(json.content);
					    		$$("div.block-layered-nav").each(function(item) {
					    			item.replace(json.leftnav);
					    		}
					    		);
					    		initSlider();
								hideLoadingBox();
					    	} , 300);        
			    	} 
			 });
	 return false;
}

function showLoadingBox()
{

	var height = $(document.body).getHeight();
	$(document.body).setStyle({
		  overflow: 'hidden',
		});
	
	if ($$('#category_products_overlay').size() == 0) {
		var category_products_overlay = document.createElement('div');
		category_products_overlay.setAttribute('id', 'category_products_overlay');
		category_products_overlay.setAttribute('class', 'ui-widget-overlay');
		category_products_overlay.hide();
		document.body.appendChild(category_products_overlay);
	}
	
	if ($$('#category_products_shadow').size() == 0) {
		var category_products_shadow = document.createElement('div');
		category_products_shadow.setAttribute('id', 'category_products_shadow');
		category_products_shadow.setAttribute('class', 'ui-widget-shadow');
		category_products_shadow.hide();
		document.body.appendChild(category_products_shadow);
	}
	
	if ($$('#category_products_overlay_text').size() == 0) {
		var category_products_overlay_text = document.createElement('div');
		category_products_overlay_text.setAttribute('id', 'category_products_overlay_text');
		category_products_overlay_text.setAttribute('class', 'ui-widget-content ui-corner-all');
		category_products_overlay_text.hide();
		document.body.appendChild(category_products_overlay_text);
		category_products_overlay_text.innerHTML = 'Loading...';
	}

	$('category_products_overlay').show();	
	$('category_products_shadow').appear(100);
	$('category_products_overlay_text').appear(100);
	
	$('category_products_overlay').style.height = height + 'px';
	
}

function hideLoadingBox()
{
	$(document.body).setStyle({
		  overflow: 'auto',
		});
	$('category_products_overlay').fade(100);
	$('category_products_shadow').fade(100);
	$('category_products_overlay_text').fade(100);
}



function initSlider()
{
	$$("div.slider-range").each(function(item) {
		var code = item.id.replace('slider_range_', "");
		var handles = ['slider_handle_min_' + code, 'slider_handle_max_' + code];
		var square_slider = new Control.Slider(handles, 'slider_range_' + code, {
		    range: $R(0, catAjaxNavSliderData[code]['max']),
		    values: $R(0, catAjaxNavSliderData[code]['max']),
		    sliderValue: [catAjaxNavSliderData[code]['currentMin'], catAjaxNavSliderData[code]['currentMax']],
		    //spans: ["square_slider_span"],
		    restricted: true,
		    onSlide: function(values) {
				$( 'slider_amount_' + code ).value = values[0] 
					+ ', ' + values[1];
		    },
		    onChange: function(values) { 
		    	var params = {};
				params[code] = '' + values[ 0 ] + ',' + values[ 1 ];
				setLayerAjaxLocation(sliderCatAjaxNavUrl, params); 
		    }
		});
		$( 'slider_amount_' + code ).value = catAjaxNavSliderData[code]['currentMin'] 
		    + ', ' + catAjaxNavSliderData[code]['currentMax'];
	});
	
	$$("div.slider-set").each(function(item) {
		var code = item.id.replace('slider_set_', "");
		var handles = ['slider_handle_min_' + code, 'slider_handle_max_' + code];
		var square_slider = new Control.Slider(handles, 'slider_set_' + code, {
		    range: $R(0, catAjaxNavSliderData[code]['max']),
		    values: $R(0, catAjaxNavSliderData[code]['max']),
		    sliderValue: [catAjaxNavSliderData[code]['currentMin'], catAjaxNavSliderData[code]['currentMax']],
		    //spans: ["square_slider_span"],
		    restricted: true,
		    onSlide: function(values) {
				$( 'slider_amount_' + code ).value = catAjaxNavSliderData[code]['values'][values[0]] 
				   + ', ' + catAjaxNavSliderData[code]['values'][values[1]];
		    },
		    onChange: function(values) { 
		    	var params = {};
				params[code] = '' + values[ 0 ] + ',' + values[ 1 ];
				setLayerAjaxLocation(sliderCatAjaxNavUrl, params); 
		    }
		});
		$( 'slider_amount_' + code ).value = catAjaxNavSliderData[code]['values'][catAjaxNavSliderData[code]['currentMin']] 
		   + ', ' + catAjaxNavSliderData[code]['values'][catAjaxNavSliderData[code]['currentMax']];
	});
	
}	
	