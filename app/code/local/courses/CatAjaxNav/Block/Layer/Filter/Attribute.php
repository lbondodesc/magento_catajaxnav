<?php
class Courses_CatAjaxNav_Block_Layer_Filter_Attribute extends Mage_Catalog_Block_Layer_Filter_Attribute
{
    public function __construct()
    {
        parent::__construct();
         $this->setTemplate('courses/catajaxnav/catalog/layer/filter/attribute.phtml');  
    }
    
    public function getItemsText()
    {
    	return $this->_filter->getItemsText();
    }
    
    public function getFilterMaxValue()
    {
    	return count($this->getItemsText()) - 1;
    }
    
	public function getAttributeCode()
    {	
    	return $this->_filter->getAttributeModel()->getAttributeCode();
    }
    
    public function getCurrentMin()
    {
	    $request = $this->getRequest()->getParam($this->getAttributeCode());
	    	if ($request) {
	    		$param = explode(',', $request);
	    		return $param[0];
	    	} else {
	    		return 0;
	    	}
    }
    
	public function getCurrentMax()
    {
	    $request = $this->getRequest()->getParam($this->getAttributeCode());
		    	if ($request) {
		    		$param = explode(',', $request);
		    		return $param[1];
		    	} else {
		    		return $this->getFilterMaxValue();
		    	}
    }
   
}
