<?php

class Courses_CatAjaxNav_Block_Layer_Filter_Decimal extends Mage_Catalog_Block_Layer_Filter_Decimal
{
    
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('courses/catajaxnav/catalog/layer/filter/decimal.phtml');
    }
    
    /*
    public function getFilterMinDecimal()
    {
    	return $this->_filter->getMinValue();
    }*/
	
    public function getCurrentMin()
    {
    	$request = $this->getRequest()->getParam($this->getAttributeCode());
    	if ($request) {
    		$param = explode(',', $request);
    		return $param[0];
    	} else {
    		return 0;
    	}
    }
    
    public function getCurrentMax()
    {
    	$request = $this->getRequest()->getParam($this->getAttributeCode());
    	if ($request) {
    		$param = explode(',', $request);
    		return $param[1];
    	} else {
    		return $this->getFilterMaxValue();
    	}
    }
    
    public function getFilterMaxValue()
    {
    	return $this->_filter->getFilterMaxValue();	
    }
    
	public function getUrl()
    { 
        return Mage::getUrl('*/*/*', array('_current'=>true, '_use_rewrite'=>true));
    }
	
    public function getAttributeCode()
    {	
    	return $this->_filter->getAttributeModel()->getAttributeCode();
    }
}
