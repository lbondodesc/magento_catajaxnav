<?php

class Courses_CatAjaxNav_Block_Layer_Filter_Category extends Mage_Catalog_Block_Layer_Filter_Category
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('courses/catajaxnav/catalog/layer/filter.phtml');
    }
}
