<?php
class Courses_CatAjaxNav_Block_Layer_Filter_Price extends Mage_Catalog_Block_Layer_Filter_Price
{
    /**
     * Initialize Price filter module
     *
     */
    public function __construct()
    {
        parent::__construct();
		$this->setTemplate('courses/catajaxnav/catalog/layer/filter/price.phtml');
    }
    
    public function getFilterMaxValue()
    {
    	return $this->_filter->getFilterMaxValue();
    }
	

   /* 
    public function getCurrentSliderPrices()
    {
    	$request = $this->getRequest()->getParam('price');
    	if ($request) {
    		
    		$prices = array ('min' => $param[0], 'max' => $param[1]);	
    		return $prices;
    	}
    	return array();	
    }
   */ 
    public function getCurrentMin()
    {
    	$request = $this->getRequest()->getParam($this->getAttributeCode());
    	if ($request) {
    		$param = explode(',', $request);
    		return $param[0];
    	} else {
    		return 0;
    	}
    }
    
	public function getCurrentMax()
    {
    	$request = $this->getRequest()->getParam($this->getAttributeCode());
    	if ($request) {
    		$param = explode(',', $request);
    		return $param[1];
    	} else {
    		return $this->getFilterMaxValue();
    	}
    }
    
    public function getAttributeCode()
    {	
    	return $this->_filter->getAttributeModel()->getAttributeCode();
    }
	
    
}
