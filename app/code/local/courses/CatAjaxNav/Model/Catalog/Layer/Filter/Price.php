<?php
class Courses_CatAjaxNav_Model_Catalog_Layer_Filter_Price extends Mage_Catalog_Model_Layer_Filter_Price
{
   
          
	public function apply(Zend_Controller_Request_Abstract $request, $filterBlock)
    {

    	$filter = $request->getParam($this->getAttributeModel()->getAttributeCode());
        if (!$filter) {
            return $this;
        }

        $filter = explode(',', $filter);
        if (count($filter) != 2) {
            return $this;
        }

        list($min, $max) = $filter;
        if (is_numeric($min) && is_numeric($max)) {
            $this->setPriceRange((int)$max);
            $this->_applyToCollection($min, $max);
            if ($max  != $this->getFilterMaxValue() || $min != 0) {
	            $this->getLayer()->getState()->addFilter(
	                $this->_createItem($this->_renderRangeLabel(empty($min) ? 0 : $min, $max), $filter)
	            );
            }
            $this->_items = array();
        }
        return $this;
    }
    
	protected function _renderRangeLabel($fromPrice, $toPrice)
    {
        $store      = Mage::app()->getStore();
        $formattedFromPrice  = $store->formatPrice($fromPrice);
        if ($toPrice === '') {
            return Mage::helper('catalog')->__('%s and above', $formattedFromPrice);
        } elseif ($fromPrice == $toPrice && Mage::app()->getStore()->getConfig(self::XML_PATH_ONE_PRICE_INTERVAL)) {
            return $formattedFromPrice;
        } else {
            if ($fromPrice != $toPrice) {
                $toPrice = $toPrice;
            }
            return Mage::helper('catalog')->__('%s - %s', $formattedFromPrice, $store->formatPrice($toPrice));
        }
    }
    
	public function getCatAjaxNavMaxPriceInt()
    {
    	return $this->_getResource()->getCatAjaxNavMaxPriceInt($this);
    }
    
	public function getFilterMaxValue()
    {
    	$str = $this->getCatAjaxNavMaxPriceInt();
    	settype($str, 'string');
    	$value = intval(str_pad(ceil($str{0} . '.' . substr($str, 1)), strlen($str), '0', STR_PAD_RIGHT));
    	return $value;
    }
}
