<?php

class Courses_CatAjaxNav_Model_Catalog_Layer_Filter_Decimal extends Mage_Catalog_Model_Layer_Filter_Decimal
{
   
    
	public function apply(Zend_Controller_Request_Abstract $request, $filterBlock)
    {
        $code = $this->getAttributeModel()->getAttributeCode();
        /**
         * Filter must be string: $index,$range
         */
        $filter = $request->getParam($code);
        if (!$filter) {
            return $this;
        }

        $filter = explode(',', $filter);
        if (count($filter) != 2) {
            return $this;
        }

        list($min, $max) = $filter;

        if (is_numeric($min) && is_numeric($max)) {
            $this->setDecimalRange((int)$range);
            $this->_getResource()->applyFilterToCollection($this, $min, $max);
            if ($max != $this->getFilterMaxValue() || $min != 0) {
	            $this->getLayer()->getState()->addFilter(
	                $this->_createItem($this->_renderItemLabel($min, $max), $filter)
	            );
            }

            //$this->_items = array();
        }
        return $this;
    }

   
    protected function _renderItemLabel($min, $max)
    {
        $from   = $min;
        $to     = $max;
        return Mage::helper('catalog')->__('%s - %s', $from, $to);
    }


    function getCatAjaxNavMaxDecimal()
    {
    	return $this->_getResource()->getCatAjaxNavMaxDecimal($this);
    }
    
	public function getFilterMaxValue()
    {
    	/*
    	$str = $this->getCatAjaxNavMaxDecimal();
    	settype($str, 'string');
    	$value = doubleval((str_pad(ceil($str{0} . '.' . substr($str, 1)), strlen($str), '0', STR_PAD_RIGHT)));
    	return $value;
    	*/
    	return ceil($this->getCatAjaxNavMaxDecimal());
    }

    
}
