<?php
class Courses_CatAjaxNav_Model_Catalog_Layer_Filter_Attribute extends Mage_Catalog_Model_Layer_Filter_Attribute
{
	
	public function getItemsText()
    {
    	$items = $this->_getItemsData();
    	$data = array();
    	foreach ($items as $key => $value) {
    		$data[] = $value['label'];
    	}
    	return $data;
    }
    
    public function getItemsId()
    {
    	$data = array();
    	foreach ($this->_getItemsData() as $key => $value) {
    		$data[] = $value['value'];
    	}
    	return $data;
    }
    
	public function apply(Zend_Controller_Request_Abstract $request, $filterBlock)
    {
        $filter = $request->getParam($this->getAttributeModel()->getAttributeCode());
    	
        if (!$filter) {
            return $this;
        }
        
        $filter = explode(',', $filter);
        if (count($filter) != 2) {
            return $this;
        }
	
        $id = $this->getItemsId();
        list($first, $second) = $filter;
        $first = $filter[0];
        $second = $filter[1];
        //$filter = array($first, $second);
        //sort($filter);
        $filterData = array();
		foreach ($this->getItemsId() as $key => $value) {
			if ($key >= $first && $key <= $second) {
				$filterData[] = $value; 
			}
		}
		$filterLabel = array();
   		foreach ($this->getItemsText() as $key => $value) {
			if ($key >= $first && $key <= $second) {
				$filterLabel[] = $value; 
			}
		}
			$text = implode(',', $filterLabel);
        //if (is_numeric($min) && is_numeric($max)) {
        $itemsId = $this->getItemsId();
        
         	$this->_getResource()->applyFilterToCollection($this, $filterData);
         	if ($itemsId[$first] != $itemsId[0] || $itemsId[$second] != $itemsId[count($itemsId)-1]) {
         		$this->getLayer()->getState()->addFilter($this->_createItem($text, $filter));
         	}
       // }
        /*
        if (is_array($filter)) {
            return $this;
        }
        $text = $this->_getOptionText($filter);
        if ($filter && strlen($text)) {
            $this->_getResource()->applyFilterToCollection($this, $filter);
            $this->getLayer()->getState()->addFilter($this->_createItem($text, $filter));
            $this->_items = array();
        }
        */
        $this->getCategoryAttributeOptions();
        return $this;
    }
    
    public function _getItemsData()
    {
        $attribute = $this->getAttributeModel();

        $options = $attribute->getSource()->getAllOptions();
    	$data = array();
   		 foreach ($options as $option) {
                if (empty($option['value'])) {
                    continue;
                }
                $data[] = array(
                            'label' => $option['label'],
                            'value' => $option['value'],
                            'count' => isset($optionsCount[$option['value']]) ? $optionsCount[$option['value']] : 0,
                        ); 
   		 }
   		 $currentDataKeys = array_keys($this->getCategoryAttributeOptions());
   		 $currentCategoryAttribute = array();
   		 foreach ($data as $key => $value) {
   		 	if (in_array($value['value'], $currentDataKeys)) {
   		 		$currentCategoryAttribute[] = $value;
   		 	}
   		 }
         return $currentCategoryAttribute;
    } 
    
 	public function getCategoryAttributeOptions()
    {
    	
    	 return $this->_getResource()->getCount($this);
    }
	
}
