<?php
class Courses_CatAjaxNav_Model_Catalog_Resource_Layer_Filter_Decimal extends Mage_Catalog_Model_Resource_Layer_Filter_Decimal
{
   
    
    public function getCatAjaxNavMaxDecimal($filter)
    {
		$category = Mage::registry('current_category');

        $collection = Mage::getResourceModel('catalog/product_collection')
            ->setStoreId($category->getStoreId())
            ->addCategoryFilter($category);
        
        $collection
            ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            //->addStoreFilter()
            ->addUrlRewrite($category->getId());

        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

        $select = clone $collection->getSelect();
        // reset columns, order and limitation conditions
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->reset(Zend_Db_Select::ORDER);
        $select->reset(Zend_Db_Select::LIMIT_COUNT);
        $select->reset(Zend_Db_Select::LIMIT_OFFSET);

        $attributeId = $filter->getAttributeModel()->getId();
        $storeId     = $collection->getStoreId();
		
        $select->join(
            array('decimal_index' => $this->getMainTable()),
            'e.entity_id = decimal_index.entity_id'.
            ' AND ' . $this->_getReadAdapter()->quoteInto('decimal_index.attribute_id = ?', $attributeId) .
            ' AND ' . $this->_getReadAdapter()->quoteInto('decimal_index.store_id = ?', $storeId),
            array()
        );
        
         $select->columns(array(
            'min_value' => new Zend_Db_Expr('MIN(decimal_index.value)'),
            'max_value' => new Zend_Db_Expr('MAX(decimal_index.value)'),
        ));
        $adapter    = $this->_getReadAdapter();
        $result = $adapter->fetchRow($select);
        //return array($result['min_value'], $result['max_value']);
        return doubleval($result['max_value']);
        
    }
    
 	public function applyFilterToCollection($filter, $range, $index)
    {
        $collection = $filter->getLayer()->getProductCollection();
        $attribute  = $filter->getAttributeModel();
        $connection = $this->_getReadAdapter();
        $tableAlias = sprintf('%s_idx', $attribute->getAttributeCode());
        $conditions = array(
            "{$tableAlias}.entity_id = e.entity_id",
            $connection->quoteInto("{$tableAlias}.attribute_id = ?", $attribute->getAttributeId()),
            $connection->quoteInto("{$tableAlias}.store_id = ?", $collection->getStoreId())
        );

        $collection->getSelect()->join(
            array($tableAlias => $this->getMainTable()),
            implode(' AND ', $conditions),
            array()
        );
     
        $min = $range;
        $max = $index;

        $collection->getSelect()
            ->where("{$tableAlias}.value >= ?", $min)
            ->where("{$tableAlias}.value <= ?", $max);
            //->group('e.entity_id')
        return $this;
    }

   
}
