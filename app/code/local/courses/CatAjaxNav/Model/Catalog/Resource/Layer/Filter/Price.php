<?php

class Courses_CatAjaxNav_Model_Catalog_Resource_Layer_Filter_Price extends Mage_Catalog_Model_Resource_Layer_Filter_Price
{

	public function applyFilterToCollection($filter, $range, $index)
    {
    	$min = $range;
    	$max = $index;
        $collection = $filter->getLayer()->getProductCollection();
        $collection->addPriceData($filter->getCustomerGroupId(), $filter->getWebsiteId());
        $select     = $collection->getSelect();
        $response   = $this->_dispatchPreparePriceEvent($filter, $select);
        $table      = $this->_getIndexTableAlias();
        $additional = join('', $response->getAdditionalCalculations());
        $rate       = $filter->getCurrencyRate();
        $priceExpr  = new Zend_Db_Expr("(({$table}.min_price {$additional}) * {$rate})");
        $select
            ->where($priceExpr . ' >= ' . $min)
            ->where($priceExpr . ' <= ' . $max);
        return $this;
    }
	
    public function getCatAjaxNavMaxPriceInt($filter)
    {
    	$category = Mage::registry('current_category');
        $collection = Mage::getResourceModel('catalog/product_collection')
            ->setStoreId($category->getStoreId())
            ->addCategoryFilter($category);
		$collection
            ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            //->addStoreFilter()
            ->addUrlRewrite($category->getId());
            Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        	Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
        
    	//$collection = $filter->getLayer()->getProductCollection();
       // $collection->addPriceData($filter->getCustomerGroupId(), $filter->getWebsiteId());

        // clone select from collection with filters
        $select = clone $collection->getSelect();
        // reset columns, order and limitation conditions
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->reset(Zend_Db_Select::ORDER);
        $select->reset(Zend_Db_Select::LIMIT_COUNT);
        $select->reset(Zend_Db_Select::LIMIT_OFFSET);
        //$select     = $this->_getSelect($filter);
        $connection = $this->_getReadAdapter();
        $response   = $this->_dispatchPreparePriceEvent($filter, $select);

        $table = $this->_getIndexTableAlias();

        $additional   = join('', $response->getAdditionalCalculations());
        $maxPriceExpr = new Zend_Db_Expr("MAX({$table}.min_price {$additional})");

        $select->columns(array($maxPriceExpr));
        return $connection->fetchOne($select) * $filter->getCurrencyRate();
    }
   
}
